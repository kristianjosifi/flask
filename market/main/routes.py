from flask import render_template, request, Blueprint
from market.models import Post
from flask_login import login_user, current_user, logout_user, login_required

main=Blueprint('main',__name__)


@main.route("/market")
@login_required
def market_page():
    page=request.args.get('page',1,type=int)
    items=Post.query.order_by(Post.date_posted.desc()).paginate(page=page,per_page=5)
    return render_template('market.html',items=items)


@main.route("/")
@main.route('/home')
def home():
    return render_template('home.html')